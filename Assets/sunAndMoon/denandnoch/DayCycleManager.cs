﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayCycleManager : MonoBehaviour
{
    [Range(0 , 1)]
    public float TimeOfDay;
    public float DayDuration = 30f;

    public AnimationCurve SunCurve; // график изменения светимости солнца
    public AnimationCurve MoonCurve;
    public AnimationCurve SkyBoxCerve;//изменение скайбокса

    public Material DaySkyBox;
    public Material NightSkyBox;
   
    public Light Sun;
    public Light Moon;

    private float sunIntesity;
    private float moonIntesity;

    void Start()
    {
        sunIntesity = Sun.intensity;
        moonIntesity = Moon.intensity;
    }

    // Update is called once per frame
     void Update()
    {
        TimeOfDay += Time.deltaTime / DayDuration;
        if (TimeOfDay >= 1) TimeOfDay -= 1;

      //  RenderSetting.skybox.Lirp(NightSkyBox, DaySkyBox, SkyBoxCerve.Evaluate(TimeOfDay));
      //  DinamicGI.UpdateEnviroment();

        Sun.transform.localRotation = Quaternion.Euler(TimeOfDay * 360f, 180, 0);
        Moon.transform.localRotation = Quaternion.Euler(TimeOfDay * 360f + 180f, 180, 0);

        Sun.intensity = sunIntesity * SunCurve.Evaluate(TimeOfDay);
        Moon.intensity = moonIntesity * MoonCurve.Evaluate(TimeOfDay);
        

    }
}
