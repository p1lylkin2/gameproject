﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTime : MonoBehaviour
{
    public float GameSecond;
    public float GameMinutes;
    //public float GameHours;

    string stringSecond;
    string stringMinutes;
    //string stringHours;

    public Text TextTime;

    // Update is called once per frame
    void Update()
    {
        GameSecond = GameSecond + Time.deltaTime;

        stringSecond = GameSecond.ToString();
        stringMinutes = GameMinutes.ToString();
      //  stringHours = GameHours.ToString();

        TextTime.text = "Время" /*+ stringHours ":" */+ stringMinutes + ":" + stringSecond;
        
        if (GameSecond >= 60f) {
            GameMinutes = GameMinutes + 1.0f;
            GameSecond = 0.0f;
        }

        /*if (GameMinutes >= 60f) {
            GameHours = GameHours + 1.0f;
            GameMinutes = 0.0f;
        }*/
    }
}
