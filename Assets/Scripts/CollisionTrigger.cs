﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTrigger : MonoBehaviour
{
    public GameObject TriggerObject;
    private float TimeOfBegin;
    public float Duration = 5f;
    public GameObject WarningMessage;
    private bool IsTriggerOn = false;

    void OnTriggerEnter(Collider collider){
        if(collider.tag == "Player"){
            EnemyManager.SetIsTriggerOnBridge(true);
            WarningMessage.SetActive(true);
            TimeOfBegin = Time.time;
            IsTriggerOn = true;
        }
    }

    void Start(){
        WarningMessage.SetActive(false);
    }
    
    void Update(){        
        if(Time.time >= this.TimeOfBegin + this.Duration && IsTriggerOn){
            WarningMessage.SetActive(false);
            Destroy(TriggerObject.GetComponent<CollisionTrigger>());
        }
    }
}
