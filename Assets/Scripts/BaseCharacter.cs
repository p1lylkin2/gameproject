﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// base class for player character and enemy character
abstract public class BaseCharacter : MonoBehaviour
{
    public float maxHealth = 100;
    public float currentHealth;

    public Transform attackPoint;
    public float attackRange;
    public Animator animator;
    public int attackDamage = 25;
    [Range(1, 10)] [SerializeField] public int DefenceSkills = 1;
    
    protected virtual void SetCurrentHealth(float NewHealth){
        currentHealth = NewHealth;
    }

    public virtual void takeDamage(float damage)
    {
        currentHealth -= damage;
        if (animator)
        {
            animator.SetTrigger("GetDamage");
        }
        if (currentHealth <= 0)
        {
            die();
        }
    }

    protected virtual void die()
    {
        if (animator)
        {
            animator.SetBool("Dead", true);
        }
    }

    public abstract void Attack();

}