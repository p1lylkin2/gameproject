﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersController : MonoBehaviour
{
    
    float speed = 2f;
    float gravity = 20f;

    Vector3 direction;
    CharacterController controller;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
    
        if(controller.isGrounded){
            direction = new Vector3 (x, 0f, z);
            direction = transform.TransformDirection (direction) * speed;
        }

        direction.y -= gravity * Time.deltaTime;
        controller.Move (direction * Time.deltaTime);
    }
}
