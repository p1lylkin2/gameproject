﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class pointanamy : MonoBehaviour
{
    private GameObject point;
    public string tag_point;
    NavMeshAgent nav;
    // Update is called once per frame
    void Update()
    {
        point = GameObject.FindGameObjectWithTag (tag_point);
        nav = GetComponent<NavMeshAgent> ();
        nav.enabled = true;
        gameObject.GetComponent<Animator> ().SetBool ("walk",true);
        nav.SetDestination (point.transform.position);
    }
}
